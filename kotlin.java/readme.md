[remark]:<class>(center, middle)
# Přehled nejzajímavějších funkci pro Java vývojáře

[remark]:<slide>(new)
## Java Interoperability
Kotlin je 100% interoperabilní s Javou.

Pomocí Kotlin můžete doslova pokračovat v práci na svých starých projektech Java.

Všechny vaše oblíbené rámce jazyka Java jsou stále k dispozici a jakýkoli rámec, který napíšete v Kotlinu, snadno přijme váš tvrdohlavý přítel milující Java.

[remark]:<slide>(new)
### Použití Javy z Kotlinu
V balíčku `km.kotlin.java.lesson` vytvořte třídu [`Person`](src/main/java/km/kotlin/java/lesson/Person.java).

```java
public class Person {

   private String name;

   // constructor s name
   // gettes and setters
   // equals and hashCode
   // toString
}
```

[remark]:<slide>(wait)
Vytvoříme soubor [`UseJava.kt`](src/main/kotlin/km/kotlin/java/lesson/UseJava.kt).

```kotlin
package km.kotlin.java.lesson

fun main() {
   val person = Person("Pepa")
   println("Person ${person} has name: " + person.getName())

   person.name = "Jirka"
   println("Person has new name ${person.name}")
}
```

[remark]:<slide>(new)
### Klíčová slova
Kotlin má více klíčových slov než Java, například `is`

Do třídy [`Person`](src/main/java/km/kotlin/java/lesson/Person.java) přidejte metodu `is(String role)`.
```java
public class Person {

   public boolean is(String role) {
       return role.charAt(0) == name.charAt(0);
   }
}
```

[remark]:<slide>(wait)
Metodu použijete v souboru [`UseKotlinKeyWorld.kt`](src/main/kotlin/km/kotlin/java/lesson/UseKotlinKeyWorld.kt).

```kotlin
fun main() {
   val person = Person("Carl")
   println("Person ${person.name} is CUSTOMER: " + person.`is`("CUSTOMER"))
}
```

[remark]:<slide>(new)
### Použití Kotlinu z Javy
Použití Korlinu z Javy je velmi podobné jako naopak, jsou te jistá specifika:

[remark]:<slide>(wait)
#### 1. Properties
Pro properties Kotlinových tříd se v Javě vygenerují příslušné `get` a `set` metody:

Vytvořte třídu [User](src/main/kotlin/km/kotlin/java/lesson/User.kt):
```kotlin
class User {

   var name: String? = null
  
}
```

[remark]:<slide>(wait)
Její property name používáme ve třídě [UseKotlinProperties](src/main/java/km/kotlin/java/lesson/UseKotlinProperties.java):
```java
public class UseKotlinProperties {

   public static void main(String[] args) {
       User user = new User();
       user.setName("Pepa");
       System.out.format("User name: %s%n", user.getName());
   }
}

```

[remark]:<slide>(new)
#### 2. Funkce
Kotlin umožňuje definovat funkce bez třídy, přímo v balíčku.

V takovém případě se v javě vytváří speciální třída se statickými metodami a s jménem odvozeným od názvu souboru.

Například v souboru [mathFunction.kt](src/main/kotlin/km/kotlin/java/lesson/mathFunction.kt):
```kotlin
package km.kotlin.java.lesson

fun plus(a: Int, b: Int): Int {
   return a + b
}
```

[remark]:<slide>(wait)
Tuto funkci pak zavoláme v Java tříde [CallPlusFunction](src/main/java/km/kotlin/java/lesson/CallPlusFunction.java)
```java
public class CallPlusFunction {

   public static void main(String[] args) {
       System.out.format("1 + 2 = %d%n", MathFunctionKt.plus(1, 2));
   }
}
```

[remark]:<slide>(new)
#### 2+. Definovaná jména pomocné třídy
Také můžeme použít annotaci `@file:JvmName("MathUtils")` pro nahrazení automatického jména generované třídy.

Například v souboru [mathFunctionNamed.kt](src/main/kotlin/km/kotlin/java/lesson/mathFunctionNamed.kt):
```kotlin
@file:JvmName("MathUtils")
package km.kotlin.java.lesson

fun plus(a: Int, b: Int): Int {
   return a + b
}
```

[remark]:<slide>(wait)
Tuto funkci pak zavoláme v Java třídě [CallPlusFunctionNamed](src/main/java/km/kotlin/java/lesson/CallPlusFunctionNamed.java):
```java
public class CallPlusFunction {

   public static void main(String[] args) {
       System.out.format("1 + 2 = %d%n", MathUtils.minus(1, 2));
   }
}
```

[remark]:<slide>(new)
#### 3. atributy třídy
Kotlin automagicky zapouzdřuje všechny atributy tříd, pokud je chceme zpřístupnit pro Javu, pak použijeme anotaci `@JvmField`.

Například máme třídu [`Pet`](src/main/kotlin/km/kotlin/java/lesson) u které chceme definovat veřejný atribute `ID`.

```kotlin
class Pet(id: String) {
   @JvmField val ID = id;
}
```

[remark]:<slide>(wait)
A tento atribut použit ve třídě [`PetClient`](src/main/java/km/kotlin/java/lesson/PetClient.java).
```java
public class PetClient {
   public String getID(Pet pet) {
       return pet.ID;
   }
}
```

[remark]:<slide>(new)
#### 4. statické atributy a třídy
Kotlin nemá statické atributy a metody.
Pokud je chceme k Kotlinu nadeklarovat pomoci *named object* nebo *companion object* a použít, anotaci `@JvmField` a `@JvmStatic`.

Příklad použití statického atributu ve třídě [`Product`](src/main/kotlin/km/kotlin/java/lesson/Product.kt) napsané v Kotlinu.
```kotlin
data class Product(var price: Double) {
   companion object {
       @JvmField
       val COMPARATOR: Comparator<Product> = compareBy<Product> { it.price}

       @JvmStatic
       fun setSale(product: Product, sale: Double) {
           product.price *= (1.0-sale)
       }
   }
}
```

[remark]:<slide>(wait)
Použití pak ve Java třídě [`ProductClient`](/src/main/java/km/kotlin/java/lesson/ProductClient.java)
```java
// get Static filed
Collections.sort(productList, Product.COMPARATOR);
System.out.format("Sorted product list %s%n", productList);

// call static method
Product.setSale(productCar, 0.3);
System.out.format("New car price %.2f%n", productCar.getPrice());
```

[remark]:<slide>(new)
#### 5. Default metody
V Javě máme od verze 8 `default` metodu v interface, Kotlin to opět podporuje prostřednictvím anotace `@JvmDefault`.

Příklad takové default metody je v Kotlin třídě [`Robot`](src/main/kotlin/km/kotlin/java/lesson/Robot.kt)
```kotlin
interface Robot {
   @JvmDefault fun move() { println("~walking~") }
   fun speak(): Unit
}
```

[remark]:<slide>(wait)
Pokud překlad končí chybou je potřeba povolit annotaci `@JvmDefault` pomocí parametru kompilatoru `-Xjvm-default=enable` v [build.gradle](build.gradle)
```groovy
compileKotlin {
   kotlinOptions.jvmTarget = "1.8"
   kotlinOptions.freeCompilerArgs = ['-Xjvm-default=enable']
}
```

[remark]:<slide>(new)
Pak je již možné definovat implementaci interface v Javě, jak je ukázáno ve třídě [`C3PO`](src/main/java/km/kotlin/java/lesson/C3PO.java)
```java
public class C3PO implements Robot {
   // move() implementation from Robot is available implicitly
   @Override
   public void speak() {
       System.out.println("I beg your pardon, sir");
   }

   public static void main(String[] args) {
       C3PO c3po = new C3PO();
       c3po.move(); // default implementation from the Robot interface
       c3po.speak();
   }
}
```

[remark]:<slide>(new)
#### 6. Přetěžování metod
Kotlin má takzvané *defaultní parametry* a nepodporuje přetěžování metod.
Pokud chceme toho efektu pro Javu dosáhnout, musíme použít annotaci `@JvmOverloads`

Jednoduchý příklad ve Kotlin Třídě [`Circle`](src/main/kotlin/km/kotlin/java/lesson/Circle.kt):
```kotlin
class Circle {

   @JvmOverloads
   constructor(centerX: Int, centerY: Int, radius: Double = 1.0) {
       println("x: ${centerX}, y: ${centerY}, r: ${radius}")
   }

   @JvmOverloads
   fun draw(label: String, lineWidth: Int = 1, color: String = "RED") {
       println("l: ${label}, lw: ${lineWidth}, c: ${color}")
   }
}
```

[remark]:<slide>(wait)
A pak použití vygenerovaných metod ve třídě [`CircleCLient`](src/main/java/km/kotlin/java/lesson/CircleClient.java)
```java
new Circle(10, 20, 30);
new Circle(1, 2);

Circle circle = new Circle(0,0);
circle.draw("aaa");
circle.draw("bbb", 10);
circle.draw("ccc", 10, "GREEN");
```

[remark]:<slide>(new)
## Známá syntaxe
Jeho syntaxe je známa každému programátorovi, který pochází z domény OOP, a může být více či méně pochopen od začátku.

Existují samozřejmě některé rozdíly od Java, jako například v konstruktorech nebo deklarace proměnných `val` a `var`.

Úryvek níže představuje většinu základů:
```kotlin
class Foo {

   val b: String = "b"     // val means unmodifiable
   var i: Int = 0          // var means modifiable

   fun hello() {
       val str = "Hello"
       print("$str World")
   }

   fun sum(x: Int, y: Int): Int {
       return x + y
   }

   fun maxOf(a: Float, b: Float) = if (a > b) a else b

}
```

[remark]:<slide>(new)
### Definice třídy
Asi mluví samo za sebe:
```kotlin
class Foo {
  
}
```

[remark]:<slide>(wait)
Nutno dodat že v kotlinu existují kromě `interface` a `enum` také datové třídy, objekty a další vychytávky...

[remark]:<slide>(wait)
### Definice metody
Tak tady je vše jasné a dobře čitelné (snad až na obrácené definice typů ...)

```kotlin
fun sum(x: Int, y: Int): Int {
   return x + y
}
```

[remark]:<slide>(new)
### Milované/nenáviděné `val` a `var`
Tyto klíčová slova se dostávají i do Javy a způsobila vlnu emocí.

Pro defakto si místo nich můžeme představit:

- `var` (variable): jako `private`.
- `val` (value): jako `private final`.

Datové typy se píši za `:` až za jménem proměnné.

```kotlin
val b: String = "b"
```

- Zlí jazykové tvrdí, že to připomíná *Pascal*.
- Naopak se dá také říct, že to sjednoduje syntaxi s UML.

![](media/uml.png)

[remark]:<slide>(new)
## String Interpolation
Je to, jako kdyby byla do jazyka zabudována chytřejší a čitelnější verze Java String.format():

```kotlin
val x = 4
val y = 7
print("sum of $x and $y is ${x + y}")  // sum of 4 and 7 is 11
```

[remark]:<slide>(wait)
V rámci výrazu `${}` můžeme použít i vlastnosti a metody objektů
```kotlin
val person = Person("Pepa")
println("Jmeno osoby: ${person.name}.")
println("Role is host: ${person.`is`("Host")}")
```

[remark]:<slide>(wait)
Pokud ale chcete použít složitější formátování:
```kotlin
val PI = Math.PI;
println("PI value: %.2f".format(PI))
```

Nebo použít další vlastnosti Kotlinu, [například zde](src/main/kotlin/km/kotlin/java/lesson/Format.kt).

[remark]:<slide>(new)
### Víceřádkové řetězce
Toto známe z Pythonu, víceřádkový `String` lze definovat pomocí `"""`.

Příklad na generování [Json](src/main/kotlin/km/kotlin/java/lesson/Json.kt)
```kotlin
data class Coordinate(val lat: Double, val lng: Double)

fun main() {
    val coordinates = listOf(
            Coordinate(42.3, 12.3),
            Coordinate(52.1, 9.8)
    )
    val json = """
        {
          "id": "${UUID.randomUUID()}",
          "coordinates": [ ${coordinates.joinToString { """
             [
                "lat": ${it.lat},
                "lng": ${it.lng}
             ]""" }}
          ]
        }
    """.trimIndent()

    println(json)
}
```

[remark]:<slide>(new)
## Type Inference
V Kotlinu nemusíte explicitně definovat typ každé proměnné.

Cílem je zvýšit čitelnost kódu a odstraněním zbytečných "jasných" definicí.

```kotlin
// type inferred to String
val a = "abc"

// type inferred to Int
val b = 4                            

// type declared explicitly
val c: Double = 0.7                

// type declared explicitly
val d: List<String> = ArrayList()    

// Only need Iterable interface
val list: Iterable<Double> = arrayListOf(1.0, 0.0, 3.1415, 2.718)

// Type is ArrayList
val arrayList = arrayListOf("Kotlin", "Scala", "Groovy")
```

[remark]:<slide>(new)
## Smart Casts
Kompilátor Kotlinu použití operátoru `is` a automaticky provádí přetypování.

V praci to neuvěřitelně zjednodušuje život:
```kotlin
if (obj is String) {
   println("Delka stringu je ${obj.length}")
}
```

[remark]:<slide>(wait)
Pro srovnání v Javě:
```java
if (obj instanceof String) {
   String str = (String)obj;
   System.out.format("Delka stringu je %d%n", str.length())
}
```

[remark]:<slide>(wait)
Operátor `instanceof` v Javě se v Kotlinu zapisuje operátorem `is`.
V příkladu výše vidíte, že v Kotlinu nemusíte přetypovávat objekt uvnitř statementu, pokud jste ho už zkontrolovali operátorem `is`.

Nadbytečné přetypování v Javě mi nepřišlo divné, dokud jsem nezačal používat Kotlin.

[remark]:<slide>(new)
### When expressions
Další místo, kde vidíte smart cast, je ve výrazech `when`:

```kotlin
when(x) {
   is Int -> print(x + 1)
   is String -> print(x.length + 1)
   is IntArray -> print(x.sum())
}
```

[remark]:<slide>(wait)
Smart Cast dohromady s výrazem when dokáže pěkně zpřehlednit kód, mějme následující kód v Javě:
```java
private void subscribeForEvents(EventBus bus) {
   bus.toObservable().subscribe(event -> {
       if (event instanceof HeartbeatEvent) {
           broadcastPing();
       } else if (event instanceof SeatReservedEvent) {
           broadcastSeatReservedEvent((SeatReservedEvent) event);
       } else if (event instanceof SeatFreeEvent) {
           broadcastSeatFreeEvent((SeatFreeEvent) event);
       } else if (event instanceof SeatBoughtEvent) {
           broadcastSeatBoughtEvent((SeatBoughtEvent) event);
       }
   });
}
```

[remark]:<slide>(new)
#### When expressions příklad
Na příkladu výše vidíte, že příchozí events, která přišla z Event busu, musí být nejprve zkontrolovat, jestli je instancí konkrétního event objektu.

Následně je přetypování na příslušný Java objekt.

Připouštím, že v Javě to v tomto případě tak hrozně nevypadá, ale podívejte se na stejný kód v Kotlinu:
```kotlin
private fun subscribeForEvents(bus: EventBus) {
   bus.toObservable().subscribe { event ->
       when (even) {
           is HeartbeatEvent -> broadcastPing()
           is SeatReservedEvent -> broadcastSeatReservedEvent(event)
           is SeatFreeEvent -> broadcastSeatFreeEvent(event)
           is SeatBoughtEvent -> broadcastSeatBoughtEvent(event)
       }
   }
}
```

[remark]:<slide>(new)
## Intuitive Equals
Volání equals () můžete zastavit explicitně, protože operátor == nyní kontroluje strukturální rovnost:
```kotlin
val john1 = Person("John")
val john2 = Person("John")

john1 == john2    // true  (structural equality)
john1 === john2   // false (referential equality)
```

V Kotlinu existují dva typy rovnosti:
- **Strukturální rovnost** (kontrola `equals()`).
- **Referenční rovnost** (dva odkazy odkazují na stejný objekt);

[remark]:<slide>(new)
## Default Arguments
Kotlin podporuje výchozí hodnoty parametrů jako například Python.
```kotlin
fun build(title: String, width: Int = 800, height: Int = 600) {
   Frame(title, width, height)
}
```

[remark]:<slide>(new)
Pokud tedy chcete v Javě definovat různé konstruktory, které umožňují vyplnit jen určitou podmnožinu atributů, musite na to takto:
```java
public class OperatorInfoInJava {

   private final String uuid;
   private final String name;
   private final Boolean hasAccess;
   private final Boolean isAdmin;
   private final String notes;

   public OperatorInfoInJava(String uuid, String name, Boolean hasAccess,
                             Boolean isAdmin, String notes) {
       this.uuid = uuid;
       this.name = name;
       this.hasAccess = hasAccess;
       this.isAdmin = isAdmin;
       this.notes = notes;
   }

   public OperatorInfoInJava(String uuid, String name) {
       this(uuid, name, true, false, "");
   }

   public OperatorInfoInJava(String name, Boolean hasAccess) {
       this(UUID.randomUUID().toString(), name, hasAccess, false, "");
   }
}
```

[remark]:<slide>(new)
Porovnejte se stnou třídou v Kotlinu:
```kotlin
class OperatorInfoInKotlin(
       val uuid: String = UUID.randomUUID().toString(),
       val name: String,
       val hasAccess: Boolean = true,
       val isAdmin: Boolean = false,
       val notes: String = "") {
}
```

[remark]:<slide>(new)
## Named Arguments
V kombinaci s výchozími argumenty, pojmenované argumenty eliminují potřebu implementaci návrhového vzoru `builder`:
```kotlin
build("PacMan", 400, 300)                           // equivalent
build(title = "PacMan", width = 400, height = 300)  // equivalent
build(width = 400, height = 300, title = "PacMan")  // equivalent
build("PacMan", width = 400, height = 300)          // equivalent
```

[remark]:<slide>(wait)
Jak je vidět na příkladě, můžete také kombinovat pojmenované a nepojmenované parametry.

*Nejprve se musí uvést nepojmenované parametry, od prvního pojmenovaného musi být všechny následující pojmenované.*

[remark]:<slide>(wait)
**Pojmenované parametry se nedaji volat na třídách z Javy.**

[remark]:<slide>(new)
### Kombinace pojmenovaných a default parametrů
Někdy dovedede kombinovaná pojmenovaných a defaultních paramterů i ztrpčit život:
```kotlin
fun mixedRequiredAndOptional(x: Int, str: String = "Hello", y: Int) {
    ...
}

fun main(args: Array<String>) {
    mixedRequiredAndOptional(1, 2)          // won't compile, 'str' missing
    mixedRequiredAndOptional(x = 1, y =2)   // will compile, 'str' is optional
}
```

[remark]:<slide>(new)
## When Expression
Honstukce `switch` je v Kotlinu nahrazena mnohem čitelnějším a flexibilnějším výraze `when`.
```kotlin
when (x) {
   1 -> print("x is 1")
   2 -> print("x is 2")
   3, 4 -> print("x is 3 or 4")
   in 5..10 -> print("x is 5, 6, 7, 8, 9, or 10")
   else -> print("x is out of range")
}
```

[remark]:<slide>(wait)
Funguje rovněž jako výraz (tedy může vracet hodnotu).
Rovněž nemusí mít argument, pak se chová jako *if-else if chain*:
```kotlin
val res: Boolean = when {
   obj == null -> false
   obj is String -> true
   else -> throw IllegalStateException()
}
```

[remark]:<slide>(new)
## Properties
Možnost předdefinovaná chování (`set` & `get` metody) , což znamená, že můžeme přestat definovat třídy se bez bezduchých getterů a setterů.
```kotlin
class Frame {
   var width: Int = 800
   var height: Int = 600

   val pixels: Int
       get() = width * height
}
```

[remark]:<slide>(wait)
Komplexnější příklady:
```kotlin
var stringRepresentation: String
   get() = this.toString()
   set(value) {
       setDataFromString(value) // parses the string
   }
```

[remark]:<slide>(wait)
```kotlin
var counter = 0 // Note: the initializer assigns the backing field directly
   set(value) {
       if (value >= 0) field = value
   }
```

[remark]:<slide>(new)
## Konstruktory
Třída v Kotlinu může mít **primární konstruktor** a libovolné množství **sekundárních konstruktorů**.

[remark]:<slide>(wait)
### Primární konstruktor
Je součástí hlavičky třídy: jde za názvem třídy (a volitelnými parametry typu).
```kotlin
class Person constructor(firstName: String) { ... }
```

[remark]:<slide>(wait)
Pokud primární konstruktor nemá anotace ani modifikátory viditelnosti, lze klíčové slovo konstruktoru vynechat:
```kotlin
class Person(firstName: String) { ... }
```

[remark]:<slide>(new)
#### Inicializační kód
Primární konstruktor nemůže obsahovat žádný kód.

Inicializační kód lze umístit do inicializačních bloků (*může jich být více!*), které jsou předponovány klíčovým slovem `init`.

Během inicializace instance jsou bloky inicializátoru spuštěny ve stejném pořadí, v jakém se objevují v těle třídy, prokládané inicializátory vlastností:
```kotlin
class InitOrderDemo(name: String) {
   val firstProperty = "First property: $name"
  
   init {
       println("First initializer block that prints ${name}")
   }
  
   val secondProperty = "Second property: ${name.length}"
  
   init {
       println("Second initializer block that prints ${name.length}")
   }
}
```

Ve třídě [Konstruktors](src/main/java/km/kotlin/java/lesson/Konstruktors.java) je vše přepsáno do Javy. 

[remark]:<slide>(new)
#### Parametry konstruktory
Všimněte si, že parametry primárního konstruktoru lze použít v inicializačních blocích.

Mohou být také použity v inicializátoru vlastností deklarovaných v těle třídy:
```kotlin
class Customer(name: String) {
   val customerKey = name.toUpperCase()
}
```

[remark]:<slide>(wait)
#### Deklarování atributů
Pro deklarování atributů a jejich inicializaci od primárního konstruktoru má Kotlin ve skutečnosti stručnou syntaxi:
```kotlin
class Person(val firstName: String, val lastName: String, var age: Int) { ... }
```

Stejně jako běžné vlastnosti mohou být vlastnosti deklarované v primárním konstruktéru proměnlivé (`var`) nebo jen pro čtení (`val`).

[remark]:<slide>(new)
#### Modifikátory konstuktoru
Pokud konstruktor obsahuje anotace nebo modifikátory viditelnosti, je vyžadováno klíčové slovo `constructor` a modifikátory jsou před něj:
```kotlin
class Customer public @Inject constructor(name: String) { ... }
```

[remark]:<slide>(wait)
#### Generování více konstruktorů pro Java
Pokud chceme aby se pro Javu vygenerovalo více kontaktů, použijeme anotaci `@JvmOverloads`
```kotlin
class Person @JvmOverloads constructor(val name: String,
                                      val age: Int? = 0)
```

[remark]:<slide>(wait)
#### Vygenerování bez parametrického konstruktoru
Pokud potřebuje (například pro ORM framework) aby třída měla i bezparametrický konstruktor.

Pak musí mít všechny parametry primárního konstruktoru výchozí hodnoty, **kompilátor pak vygeneruje další parametr bez konstruktoru automaticky**.
```kotlin
class Person (val name: String = "", val age: Int? = 0)
```

[remark]:<slide>(new)
### Sekundární konstruktor
Třída může také deklarovat sekundární konstruktory:
```kotlin
class Person {
   var children: MutableList<Person> = mutableListOf<Person>();
  
   constructor(parent: Person) {
       parent.children.add(this)
   }
}
```

[remark]:<slide>(wait)
#### Kombinace primárního a sekundárního konstruktoru
Pokud má třída primární konstruktor, musí se každý sekundární konstruktor delegovat na primární konstruktor.
```kotlin
class Person(val name: String) {
   var children: MutableList<Person> = mutableListOf<Person>();
   constructor(name: String, parent: Person) : this(name) {
       parent.children.add(this)
   }
}
```

[remark]:<slide>(new)
#### Více sekundárních konstruktorů
sekundárních konstruktorů může být více, misí se stejně jak v Javě rozlišit parametry.
```kotlin
class MoreConstructors {
   constructor(i: Int) {
       println("Int constructor")
   }
   constructor(d: Double) {
       println("double constructor")
   }
}
```

Příklad ve třídě [MoreConstructors](src/main/kotlin/km/kotlin/java/lesson/MoreConstructors.kt)



[remark]:<slide>(new)
## The Data Class
Pokud vytváříte např. business logiku, perzistentní vrstvu apod., vytváříte většinou doménové objekty, třídy, které slouží pouze pro uchovávání dat (POJO).

Uveďme si to na následujícím příkladu.

Máme třídu Customer, která bude uchovávat data o id zákazníka, jméně a emailu.

[remark]:<slide>(wait)
#### V jazyce Java:
```java
public class Customer {
  
   private int id;
   private String name;
   private String email;

   public int getId() {
       return id;
   }

   public void setId(int id) {
       this.id = id;
   }

   public String getName() {
       return name;
   }

   public void setName(String name) {
       this.name = name;
   }

   public String getEmail() {
       return email;
   }

   public void setEmail(String email) {
       this.email = email;
   }
}

```

[remark]:<slide>(new)
Jazyk Kotlin má pro tyto účely speciální označení třídy, tzv. datová třída (data class).

#### V jazyce Kotlin:
```kotlin
data class Customer(
       val id: Int,
       val name: String,
       val email: String)
```

[remark]:<slide>(wait)
### Vygenerované metody
Kompilátor Kotlinu pak k data class vygeneruje:

- funkce `hasCode()` a `equals()`
- `toString()` ve formě `"User(name=John, age=42)"`
- přidá funkci copy() (o této funkci mluvím níže)
- funkce `componentN()`

[remark]:<slide>(new)
### Požadavky na datovou třídu
Aby byla zajištěna konzistence a smysluplné chování, musí datové třídy splňovat následující požadavky:
- **Primární Konstruktor** musí mít alespoň jeden parametr.
- Všechny parametry primárního konstruktoru musí být označeny jako `val` nebo `var`
- Třídy dat nemohou být `abstract`, `open`, `sealed` or `inner`;

Pro datové třídy dále platit:
- Pokud existují explicitní implementace `equals()`, `hashCode()` nebo `toString()` v těle datové třídy nebo `final` implementace v nadřazené třídě, pak tyto funkce nebudou vygenerovány.
- Pokud má nadřazená třída metodu `componentN()`, pak musí být `open` návratové hodnoty kompatibilní, jinak vznikne chyba.
- Není dovoleno poskytovat explicitní implementace metod `componentN()` a `copy()`.


[remark]:<slide>(new)
### Vlastnosti deklarované v těle třídy
Kompilátor generuje metody na základě parametrů definovaných uvnitř primárního konstruktoru.

Chcete-li vyloučit atribut z generovaných metod, deklarujte ji uvnitř těla třídy:
```kotlin
data class Man(var name: String) {
   var age: Int = 0
}
```

Uvnitř implementací toString (), equals (), hashCode () a copy () bude použit pouze název vlastnosti a bude existovat pouze jedna komponentní funkce `component1()`.
Zatímco dva objekty Osob mohou mít různý věk, bude s nimi zacházeno jako s rovnými.
```kotlin
val man1 = Man("John")
val man2 = Man("John")
println("${man1} == ${man2}: ${man1 == man2}")
man1.age = 10
man2.age = 20
println("${man1} == ${man2}: ${man1 == man2}")
```

Kopleknější příklad ve tříde: [Man](src/main/kotlin/km/kotlin/java/lesson/Man.kt).

[remark]:<slide>(new)
### Metoda copy
Pokud vytvoříte datovou třídu jako immutable, nebo prostě jen nechcete měnit původní objekt, pak musíte při změně atributu vytvořit kopii.

Některé jazyky pro tento účel vytvářejí klonovací konstruktory, Kotlin má metodu `copy()`.

[remark]:<slide>(wait)
Mějme immutable datovou třídu `Book`:
```kotlin
data class Book(val title: String, val autor: String, val pages: Int)
```

[remark]:<slide>(wait)
Metoda `copy()` by se dala následujícím způsobem:
```kotlin
fun copy(name: String = this.name, age: Int = this.age) = User(name, age)
```

[remark]:<slide>(wait)
Metodu za nás vygeneruje compiler, takže my ji můžeme rovnou použit:
```kotlin
   val hobit = Book("Hobit", "Tolkien", 256)
   val reprintHobit = hobit.copy(pages = 320)
```

Celý příklad v souboru [Book.kt](src/main/kotlin/km/kotlin/java/lesson/Book.kt).

[remark]:<slide>(new)
## Operator Overloading
Předdefinovaná sada operátorů může být přetížena, aby se zlepšila čitelnost:
```kotlin
data class Vector (val x: Float, val y: Float) {
   operator fun plus(v: Vector) = Vector(x + v.x, y + v.y)
}
```
Příklad použití je v souboru [Vector.kt](src/main/kotlin/km/kotlin/java/lesson/Vector.kt).

[remark]:<slide>(wait)
Přehled všch metod nalezenete v [Dokumentaci](https://kotlinlang.org/docs/reference/operator-overloading.html).
- Najdeme zde všechny tradiční operátory: `+`,`-`,`/`,`+`,`<`,`>`...
- Dále některé jazykově specifické: `in`, `..` (range)
- Ale i operátor `[]` pro práci s poli,
- nebo také `f()`: invoke fungující jako `__call__` v pythonu.
- **naopak operátory `&&` a `||` přetížit nelze**.

[remark]:<slide>(new)
### `infix` funkce
Existují tak `infix fun`, které **musí mít jeden parametr** a které nám umožní definovat vlastní operátor:
```kotlin
infix fun Int.shl(x: Int): Int { ... }

// calling the function using the infix notation
1 shl 2

// is the same as
1.shl(2)
```

Takže je možné definovat operátor `and` a `or`, ... :-)

[remark]:<slide>(new)
## Destructuring Declarations
Některé objekty mohou být rozloženy, což je užitečné například pro iterační mapy:
```kotlin
for ((key, value) in map) {
   print("Key: $key")
   print("Value: $value")
}
```

[remark]:<slide>(wait)
Za rozklad objektu odpovídá funkce `componentN()`, o které jsme mluvili například u datových tříd.

Mějme třídu [`Book`](src/main/kotlin/km/kotlin/java/lesson/Book.kt) a její instanci `book`:
```kotlin
val (title, autor, pages) = book
println(title)
println(autor)
println(pages)
```

[remark]:<slide>(wait)
Což je defakto stejné jako napsat:
```kotlin
val title = book.component1()
val autor = book.component2()
val pages = book.component3()   
```

[remark]:<slide>(new)
### Funkce vracející více hodnot
tímto způsobem je možné vytvořit funkci vracející více hodnot.

Definice funkce:
```kotlin
fun function(...): Pair<Int, Status> {
   // computations
   val result = 1
   val status = Status()
   return Pair(result, status)
}
```

Použití:
```kotlin
val (result, status) = function(...)
```
nebo přesněji:
```kotlin
val (result: Int, status: Status) = function(...)
```

[remark]:<slide>(wait)
- *Object `Pair` můžeme nahradit vlastním datovým objektem.*
- *Kromě objektu `Pair` existuje i objekt `Triple`.*

[remark]:<slide>(new)
### Zahození položky
Pokud s některou s položek nechceme dále použít, můžeme místo jména proměnné použit `_`.
```kotlin
val (_, status) = getResult()
```

[remark]:<slide>(new)
### Rozklad a lambda výrazy
Dalším velmi šikovným použitím rozkladu je v lambda výrazu:
```kotlin
{ a -> ... } // one parameter
```
[remark]:<slide>(wait)
```kotlin
{ a, b -> ... } // two parameters
```
[remark]:<slide>(wait)
```kotlin
{ (a, b) -> ... } // a destructured pair
```
[remark]:<slide>(wait)
```kotlin
{ (a, b), c -> ... } // a destructured pair and another parameter
```

[remark]:<slide>(wait)
Praktické použití je například opět u map, zvažte následující příklad:
```kotlin
map.mapValues { entry -> "${entry.value}!" }
map.mapValues { (key, value) -> "${value}!" }
```

[remark]:<slide>(wait)
#### Typová bezpečnost
```kotlin
map.mapValues { (_, value): Map.Entry<Int, String> -> "${value}!" }
map.mapValues { (_, value: String) -> "${value}!" }
```

[remark]:<slide>(new)
## Ranges
Přehlednější zápis `for` cyklu díky `range`:
```kotlin
for (i in 1..100) { ... }
for (i in 0 until 100) { ... }
for (i in 2..10 step 2) { ... }
for (i in 10 downTo 1) { ... }
if (x in 1..10) { ... }
```

[remark]:<slide>(wait)
Range mohou tvořit všechny třídy přetěžující operátor `rangeTo`.
```kotlin
val versionRange = Version(1, 11)..Version(1, 30)
println(Version(0, 9) in versionRange)
println(Version(1, 20) in versionRange)
```

[remark]:<slide>(wait)
Range generuje iterator, takže jej můžeme použít "funkcionálně":
```kotlin
println((1..10).filter { it % 2 == 0 })
```

[remark]:<slide>(new)
## Extension Functions
Kotlin nám umožňuje rozšířit funkčnost existujících tříd bez použití dědičnosti.
```kotlin
fun String.capitalize(): String {
 return this.toUpperCase()
}
```

Funkce `capitalize()` je **extension funkce** třídy `String`.

Uvnitř této extension funkce získáte po zadání klíčového slova `this` přístup k objektu, na který byla aplikována.
```kotlin
fun String.hello() {
 println("Hello, $this!")
}

fun String.and(input: String): String {
 return "${this} $input"
}
```

Extension funkce nemění třídy, které rozšiřuje.
Vše je vyřešeno staticky.
```kotlin
println("Pepa Novak".capitalize()) // prints PEPA NOVAK

"Pepa".hello() // prints 'Hello, Pepa!'

var testString = "This is a string".and("This is another")
println(testString) // prints 'This is a string This is another'
```

[remark]:<slide>(new)
Díky extension funkcím je kód v projektech, kde používám Kotlin, daleko čistější.
Vyhnu se tak totiž `xxUtils(.java)` třídám.

Dobrým příkladem pro použití funkce rozšíření by mohla být konverze na JSON.
Ukážeme si to na velmi jednoduchém příkladu, který přidává extension funkci do všech “objektů” v Kotlinu (`kotlin.Any` a `java.lang.Object` jsou různé typy, ale během spuštění jsou reprezentovány stejnou třídou `java.lang.Object`).
```kotlin
val gson = Gson()
// ...
fun Any.toJSON(): String {
 return gson.toJson(this)
}
// ...

// Now if we want to convert it to JSON we can just simple call ourObject.toJSON()
val customer = Customer(id = 2001,
                       name = "Pepa Novak",
                       email = "pepa.novak@email.com")
val json = customer.toJSON()
```

[remark]:<slide>(new)
## Extension Properties
Ne tak hezké, ale taky je možnost rozšiřování property.
```kotlin
val <T> List<T>.lastIndex: Int
   get() = size - 1
```

[remark]:<slide>(new)
## Companion Object Extensions
Pokud třída má **companion object** je možné jej rozšířit také:
```kotlin
class MyClass {
   companion object { }  // will be called "Companion"
}

fun MyClass.Companion.foo() { ... }
```

a následně volán jako:
```kotlin
MyClass.foo()
```

**Poznámka**: *companion object je vysvětlen níže.*

[remark]:<slide>(new)
## `object` jako Singleton
Další skvělou vlastností Kotlinu je jednoduchost, s jakou můžete definovat “singleton” objekty.

Použití singletonu v Javě může vypadat takto:
```java
public class SingletonInJava {
   private static SingletonInJava INSTANCE;
   // Omlouvám se za zjednodušení ...
   public static SingletonInJava getInstance() {
       if (INSTANCE == null) {
           INSTANCE = new SingletonInJava();
       }
       return INSTANCE;
   }  
}
```

[remark]:<slide>(wait)
Zápis singletonu v Kotlinu :
```kotlin
object SingletonInKotlin {

}

// And we can call
SingletonInKotlin.doSomething()
```

[remark]:<slide>(new)
### Motivace ke konstukci `object`
Někdy musíme vytvořit objekt, který ale mírně modifikuje nějakou třídu třídy.

Nechceme ale vytvářet novou samostatnou třídu.

Java zpracovává tento případ s **anonymními vnitřními třídami**.

Kotlin mírně zobecňuje tento koncept pomocí **objektových výrazů** a **deklarací objektů**.

[remark]:<slide>(wait)
#### 1. Příklad implementace posluchačů GUI:
```kotlin
window.addMouseListener(object : MouseAdapter() {
   override fun mouseClicked(e: MouseEvent) { ... }

   override fun mouseEntered(e: MouseEvent) { ... }
})
```

[remark]:<slide>(new)
#### 2. Příklad jednorázového přidání rozhraní ke třídě
Mějme naši třídu a nějaké API, které nám diktuje použití rozhraní.

Díky `object` nemusíme vytvářet novou třídu (potomak naší třídy implementující rozhraní) pro jednorázové použití:
```kotlin
open class A(x: Int) {
   public open val y: Int = x
}

interface B { ... }

val ab: A = object : A(1), B {
   override val y = 15
}
```

[remark]:<slide>(new)
#### 3. Příklad vytvoření adHoc objektu
Někdy prostě potřebuje vrátit objekt ...
```kotlin
fun foo() {
   val adHoc = object {
       var x: Int = 0
       var y: Int = 0
   }
   print(adHoc.x + adHoc.y)
}
```

[remark]:<slide>(new)
### Companion object
Deklaraci objektu uvnitř třídy lze označit klíčovým slovem `companion`:
```kotlin
class MyClass {
   companion object Factory {
       fun create(): MyClass = MyClass()
   }
}
```

[remark]:<slide>(wait)
Metody companion objektu lze zavolat jednoduše pomocí názvu třídy jako kvalifikátoru:
```kotlin
val instanceMyClass = MyClass.create()
```

[remark]:<slide>(wait)
získání instance companion objektu:
```kotlin
val x = MyClass.Companion
val y = MyClass
```

[remark]:<slide>(new)
Všimněte si, že ačkoli companion object vypadají jako statické objekty/metody z Javy, jsou jisté rozdíly:
- Stále jsou to instance klasických objektů
- mohou například implementovat rozhraní:

```kotlin
interface Factory<T> {
   fun create(): T
}

class MyClass {
   companion object : Factory<MyClass> {
       override fun create(): MyClass = MyClass()
   }
}

val f: Factory<MyClass> = MyClass
```

[remark]:<slide>(new)
## Null Safety
Java je to, co bychom měli nazvat **téměř** staticky psaným jazykem.

V něm není zaručeno, že proměnná typu `String` bude odkazovat na `String` - může odkazovat na `null`.

I když jsme na to zvyklí, neguje to bezpečnost statické kontroly typu, a v důsledku toho musí vývojáři Java žít v neustálém strachu z NPE.

*(Zas tak bych to ale nedramatizoval)*

[remark]:<slide>(new)
### Null a Kotlin
Kotlin je takzvaný null safety jazyk.
```kotlin
class Owner {

 var adress: String = ""
 var telephone: String = ""
 var email: String? = null
}
```

[remark]:<slide>(wait)
V tomto příkladě nemůžete proměnným address a telephone přiřadit hodnotu null.

V případě, že jim nastavíte hodnotu null, tak již během kompilace budete na tuto skutečnost upozorněni:

![](media/03_compilation_error.png)

[remark]:<slide>(new)
### Povolení `null` hodnoty
Pokud chcete **proměnné přiřadit hodnotu null**, pak je nutné to **explicitně definovat**.

To uděláte přidáním **“?”** za typ proměnné (viz příklad emailu jako proměnné výše).
```kotlin
var a: String = "abc"
a = null // compilation error

var b: String? = "abc"
b = null // ok

val y: String = null // Does not compile.
```

[remark]:<slide>(wait)
Pokud tedy explicitně řeknete, že proměnná může obsahovat i `null` hodnotu, tak se vám může stát, že se potkáte se starou známou `NullPointerException` (NPE), kterou jistě každý z nás moc dobře zná z Javy.
```kotlin
val x: String? = "Hi"
x.length // Does not compile.
```

[remark]:<slide>(new)
### Ošetření `NullPointerException`
Abyste vyřešili kompilační chybu `x.length`, můžete použít if-statement:
```kotlin
if (x != null) {
 x.length // Compiles! Not idiomatic just to get length!
}
```

[remark]:<slide>(wait)
Nebo to můžete udělat ještě lépe prostřednictvím tzv. **“safe calls”**, které vrátí hodnotu volání, pokud volající není nulový (jinak se vrátí `null`).
```kotlin
val len = x?.length
```

[remark]:<slide>(wait)
A naopak, můžete říci kompilatoru, že víte že proměnná nemůže být null a pak můžete použít **"non-ull"** operátor `!!`
```kotlin
val len = x!!.length // Will throw if null.
```

[remark]:<slide>(new)
### Elvis Operator
Pomocí funkce „Elvis Operator“ můžete pracovat s nulovými typy efektivněji.
```kotlin
// Elvis operator.
val len = x?.length ?: -1
```

V tomto případě bude použita length, pokud x není nulové, jinak bude použito -1 (výchozí hodnota).

[remark]:<slide>(new)
## Better Lambdas
Lambda systém je KOtlinu velmi dobře navržen.

Díky vyváženým možnostem designu dokonale vyvážený mezi čitelností a akademičností.

Syntaxe je především přímočará:
```kotlin
val sum = { x: Int, y: Int -> x + y }   // type: (Int, Int) -> Int
val res = sum(4,7)  
```

[remark]:<slide>(wait)
A tady přicházejí chytré body:
1. Závorky metod lze přesunout nebo vynechat, pokud je lambda posledním nebo jediným argumentem metody.
2. Pokud se rozhodneme nepodat prohlášení o argumentu lambda s jedním argumentem, bude to implicitně deklarovaný pod názvem `it`.

[remark]:<slide>(wait)
Tato fakta dohromady činí následující tři řádky rovnocenné:
```kotlin
numbers.filter({ x -> x.isPrime() })
numbers.filter { x -> x.isPrime() }
numbers.filter { it.isPrime() }
```

[remark]:<slide>(new)
#### Lambda příklad:
Lambda nám umožňuje psát stručný funkční kód:
```kotlin
persons
   .filter { it.age >= 18 }
   .sortedBy { it.name }
   .map { it.email }
   .forEach { print(it) }
```

[remark]:<slide>(wait)
#### Refenrece na metodu:
Toto je velmi podobné jako v Javě.

je možné použít operátor `::` jako referenci na metodu/funkci.

```kotlin
persons.forEach(::println)
```
 
[remark]:<slide>(new)
### Více-příkazový lambda výraz:
V kotlinu je možné (samozřejme) mít více příkazu v lambda výrazu:

#### 1. Pomocí `;`
Středník odděluje příkazy a narozdíl od javy je nepovinný:
```kotlin
persons
   .map { val email = it.email; email.toUpperCase() }
   .forEach { print(it) }
```

#### 2. Pomocí konce řádků
Středník odděluje příkazy a narozdíl od javy je nepovinný:
```kotlin
persons
   .map { val email = it.email
          email.toUpperCase() }
   .forEach { print(it) }
```
[remark]:<slide>(new)
### Standard Library Functions
Kotlin obsahuje několi užitečných standardních funkcí, které jsou přidány do všech tříd. 

Následující část představuje malý výňatek z toho, co považuji za nejčastěji používané.

[remark]:<slide>(wait) 
#### Funkce `let()`
`let()` vrazí (obvykle odlišný) výsledný objekt, takže jej lze považovat hlavně za transformační funkci při volání funkce řetězení. 

Příklad upravy řetězce [let.kt](src/main/kotlin/km/kotlin/java/lesson/let.kt):
```kotlin
var str = "Hello World".let { "$it!!" }
println(str)
```

[remark]:<slide>(new)
#### Kombinování `let()`
Další oblíbenou technikou je zřetězení funkcí `let()`:
```kotlin
var a = 1
var b= 2

a = a.let { it + 2 }
     .let { val i = it + b; i}
println(a)
```

[remark]:<slide>(wait)
Kromě řetězení lze funkce `let()` i zanořovat:
```kotlin
var x = "Anupam"
x = x.let { outer ->
    outer.let { inner ->
        println("Inner is $inner and outer is $outer")
        "Kotlin Tutorials Inner let"
    }
    "Kotlin Tutorials Outer let" 
}
println(x) //prints Kotlin Tutorials Outer let
```

[remark]:<slide>(new)
#### Funkce `also()`
`also()` také vždy vrací původní odkaz, což je v pořádku, když chcete jednoduše pracovat s objektem.

Kdybychom v príkladu z [also.kt](src/main/kotlin/km/kotlin/java/lesson/also.kt) použil let, výsledek by byl **3**:
```kolin
var m = 1
m = m.also { it + 1 }.also { it + 1 }
println(m) //prints 1
```

[remark]:<slide>(wait)
Nebo chcete přiřadit do proměnné hodnotu a současně chcete zalogovat.
```kotlin
val v = "New String value".also(::println)
```

[remark]:<slide>(new)
#### Rozdíl funkcí `let()` vs. `also()`
Následující příklad demonstuje hlavní rozdíl:
```kotlin
data class Person(var name: String, var tutorial : String)
var person = Person("Anupam", "Kotlin")

var personLet = person.let { it.tutorial = "Android" }
var personAlso = person.also { it.tutorial = "Android" }
    
println(person)
println(personLet)
println(personAlso)
``` 

[remark]:<slide>(new)
#### Funkce `run()`
Funkce `run()` může změnit vnější vlastnost. 

- Podobně jako funkce `let` vrací funkce `run` také poslední příkaz.
- Na rozdíl od `let` funkce `run` nepodporuje klíčové slovo `it`.

```kotlin
var tutorial = "This is Kotlin Tutorial"
println(tutorial) //This is Kotlin Tutorial
tutorial = run {
    val tutorial = "This is run function"
    tutorial
}
println(tutorial) //This is run function
```

V [run.kt](src/main/kotlin/km/kotlin/java/lesson/run.kt) jsme tedy mohli definovat lokální proměnnou.

[remark]:<slide>(new)
#### Kombinace `let()` a `run()`
Trochu praktičtější příklad je nahrazní `if-else` při testování na `null`

V Javě něco jako:
```java
String str = null;
if (str != null) {
    System.out.format("Str is %s%n", str);
} else {
    System.out.println("Str was null");
    str = "Java";
}
System.out.format("Result str: %s%n", str);
```

[remark]:<slide>(wait)
A teď trochu funkcionálně v Kotlinu:
```kotlin
var str : String? = null
str?.let { println("p is $str") } ?:
     run { println("p was null.")
           str = "Kotlin"}

println(str)
```

[remark]:<slide>(new)
#### Funkce `apply()`
`apply()` je velmi podobné `alse()`, ale používá `this` místo lambda proměné `it`.

To může být problematické, potřebujete-li přístup k vnějšímu dosahu pomocí `this`.

Příklad ze souboru [apply.tk](src/main/kotlin/km/kotlin/java/lesson/apply.kt):
```kotlin
data class Person(var name: String, var tutorial : String)
var person = Person("Anupam", "Kotlin")

person.apply { this.tutorial = "Swift" }
println(person)
```

[remark]:<slide>(wait)
Měli by jst používat `also` pouze když nechcete použít `this`.
```kotlin
data class Person(var name: String, var tutorial : String)
var person = Person("Anupam", "Kotlin")

person.apply { tutorial = "Swift" }
println(person)

person.also { it.tutorial = "Kotlin" }
println(person)
```

[remark]:<slide>(new)
#### Funkce `with()`
`with()` má podobné použití jako `apply()`.
 
Používá ke změně vlastností instance, aniž by bylo nutné pokaždé volat operátor tečky nad referencí.

Jednoduchý příklad ze souboru [with.tk](src/main/kotlin/km/kotlin/java/lesson/with.kt):
```kotlin
data class Person(var name: String, var tutorial : String)
var person = Person("Anupam", "Kotlin")

with(person)
    {
        name = "No Name"
        tutorial = "Kotlin tutorials"
    }
println(person)
```

[remark]:<slide>(new)
#### Funkce `use()`
Funkce `use()` připomíná z Javy *try-with-resources* a lze ji použít na libovolném `Closeable`.
 
Je tedy velmi užitečná v rámci I/O operaví.

Zvažte následující příklad kódu použitého v rámci *Spting framwworku*:
```kotlin
// Spring injected field require late initialization
// and mutability
@Value("classpath:file.txt")
lateinit var resource: Resource


fun handleResource() {
  resource
      .inputStream
      .use { StreamUtils.copyToString(it, Charset.defaultCharset()) }
      // stream is safely closed
      .let { str -> str.toUpperCase() }
      .also(::println)
      .run(::handleString)
}

fun handleString(str: String) {
    ...  
}
```

[remark]:<slide>(new)
## Collections
Kotlin poskyuje 3 základní typy kolekci, stejně jako Java: 
- **List**, 
- **Set**,
- **Map**.

[remark]:<slide>(wait)
Narozdíl od javy existují od každé kolekce dvě základi variany: 
- **read-only**,
- **mutable**.

[remark]:<slide>(wait)
A podobně jako v javě existují obecné třídy: 
- **Iterable**,
- **Collection**.

[remark]:<slide>(new)
#### Digram kolekcí
![](media/collections-diagram.png)


[remark]:<slide>(new)
### Vytváření kolekci:

[remark]:<slide>(wait)
#### Pomocí výčtů a funkcí:

|     | List | Set | Map |
| --- | :--- | :-- | :-- |
| **read-only** | `listOf(1, 2, ...)`| `setOf(1, 2, ...)` | `mapOf("key1" to 1, ...)`|
| **mutable** | `mutableListOf(...)` | `mutableListOf(...)` | `mutableMapOf(...)` |

[remark]:<slide>(wait)
#### Iterativně pomocí lambda:
Pomocí konstruktoru (`List`, `MutableList`)
```kotlin
val doubled = List(3, { it * 2 })
println(doubled)
```

[remark]:<slide>(wait)
#### Kopírovaním
Pomocí metod (`otList`, `toMutableList`, ...)
```kotlin
val sourceList = listOf(1, 2, 3)
val copyList = sourceList.toMutableList()

sourceList.add(4)
println("Copy size: ${copyList.size}")   
```

[remark]:<slide>(new)
## Streams and Collections
Stejně jako Java i kotlin poskytuje možnost zpracování kolekcí pomocí "streamů".

Místo metody `stream()` z Javy ale používá `asSequence()`.
 
Následující přehled uvání některé užitečné funkce:
- `map()`, `mapNotNull()`, `mapKeys()`, `mapValues​​()`: standardní transformace z jedné reprezentace do druhé pro seznamy a mapy
- `first()`, `last()`: první nebo poslední prvek odpovídající dané predikátové funkci
- `single()`: extrahuje jediný prvek odpovídající danému predikátu, pokud se shoduje více prvků, selže.
- `associate()`, `associateBy()`: transformace ze seznamu na položku mapy přidružením klíčů k hodnotám.
- `mapIndexed()`, `filterIndexed()`, `forEachIndexed()`: provádět operace pomocí další indexové proměnné, a tak se vyhnout další smyčce, jako v Javě.
- `plus()`, `drop()`, `dropLast()`: přidat nebo odebrat prvky ze seznamu.

Všechny tyto funkce ve výchozím nastavení vracejí neměnné kolekce, čímž se zajišťuje, že operace jsou bez vedlejších účinků. 

Funkce `toMutableList()` a `toMutableMap()` lze v případě potřeby použít k získání mutabilních kopií.

[remark]:<slide>(new)
## Enumerations and Sealed Classes
Kotlin poskytuje enum class, stejně jako Java: 
```kotlin
enum class Direction {
    NORTH, SOUTH, WEST, EAST
}
```

[remark]:<slide>(wait)
A stejně jako java jsou Enum objekty, které můžeme dědit ...
```kotlin
enum class ProtocolState {
    WAITING {
        override fun signal() = TALKING
    },

    TALKING {
        override fun signal() = WAITING
    };

    abstract fun signal(): ProtocolState
}
```

[remark]:<slide>(new)
### Sealed Classes
Vjavě můžou mít enumy různé chování, ale protože se jedná potomky jedné třídt, nemoho mít různé atributy.
 
V Kotlinu lze použít tzv. *sealed classes* k vytvoření mnohem flexibilnějšího výčtu.
 
Kompilátor zajistí vnoření třídy a začlení je výštu.
```kotlin
sealed class Operation {
    object Show: Operation()
    object Hide: Operation()

    data class Log(val message: String): Operation()
    data class Exit(val rc: Int): Operation()
}
```

[remark]:<slide>(new)
### Použití výčtu:
```kotlin
fun handleOp(op: Operation) {
    when(op) {
        Operation.Show -> println("show")
        Operation.Hide -> println("hide")
        is Operation.Log -> println("log: ${op.message}")
        is Operation.Exit -> System.exit(op.rc)
    }
}
```

Jak je ukázáno v příkladu, operace s uzavřenou třídou obsahuje různě prvky, včetně objektů (jako singletons). 

Klíčové slovo is je volitelné pro singletony, protože existuje vždy pouze jedna instance. 

Implicitní smart casting dále umožňuje přístup k podtypu bez nutnosti přetypování.
 
[remark]:<slide>(new)
## Dědičnost
Dědění je v Kotlinu naruby - dobré, ale je potřeba mít na paměti

Zatímco v Javě je dědění implicitně povoleno a v případě potřeby lze zakázat klíčovým slovem `final`, v Kotlinu je to naopak: 

- dědění je implicitně zakázáno,
- v případě potřeby lze povolit klíčovým slovem `open`. 

```kotlin
open class Base(p: Int)

class Derived(p: Int) : Base(p)
```

[remark]:<slide>(wait)
To znamená, že když v Kotlinu deklarujeme třídu nebo metodu bez modifikátoru `open`, je to, jako bychom v Javě tutéž třídu nebo metodu deklarovali s modifikátorem `final`.

[remark]:<slide>(new)
### Přetížení metod
To stejné platí i pro metody:
```kotlin
open class Base {
    open fun v() { ... }
    fun nv() { ... }
}
class Derived() : Base() {
    override fun v() { ... }
}
```

[remark]:<slide>(wait)
### Přetížení properties
To stejné platí i pro metody:
```kotlin
open class Base {
    open val x: Int get() { ... }
}
class Derived() : Base() {
    override val x: Int = ...
}
```

[remark]:<slide>(new)
### Volání konstruktoru předka
Paramtery pro konstukror předka se definuji:
- Za dvojtečkou po definici hlavičky konstruktor
- U primárního konstruktoru se použíje jméno předka

```kotlin
open class Base(p: Int)

class Derived(p: Int) : Base(p)
```

[remark]:<slide>(wait)
### Volání sekundárního konstruktoru předka
Můžeme volat i sekundární konstruktory, pokud potomek nemá primární konstruktor.

V takovém případě použijeme klíčové slovo `super`:
```kotlin
class MyView : View {
    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)
}
```

[remark]:<slide>(new)
### Mušlenky otce Blocha
Tento přístup naplňuje zásadu, kterou přinesl Joshua Bloch ve své knize Effective Java: dědění navrhněte a zdokumentujte, nebo je zakažte. 

* [Java efective online](https://kea.nu/files/textbooks/new/Effective%20Java%20%282017%2C%20Addison-Wesley%29.pdf) kapitola 19. pdf strana 114.

Tím, že třídy a metody v Kotlinu jsou implicitně final, získáme větší efektivitu výsledného kódu: JVM zpracovává final kód efektivněji.

Dobrá věc, ale je potřeba ji mít při programování na paměti, aby nás nepříjemně nepřekvapil kompilátor.



