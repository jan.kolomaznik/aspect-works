package km.kotlin.java.lesson

fun main() {

    data class Bool(val bool: Boolean) {
        infix fun and(other: Bool) = Bool(this.bool && other.bool)
    }

    val a = Bool(true)
    val b = Bool(false)

    println("${a} and ${b} == ${a.and(b)}")
}