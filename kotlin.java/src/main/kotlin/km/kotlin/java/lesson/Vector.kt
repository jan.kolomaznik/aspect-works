package km.kotlin.java.lesson

data class Vector (val x: Float, val y: Float) {
    operator fun plus(v: Vector) = Vector(x + v.x, y + v.y)
}

object add {
    var last: Vector? = null

    operator fun invoke(a: Vector, b: Vector): Vector? {
        this.last = a + b
        return this.last
    }
}

fun main() {
    val a = Vector(2f, 3f)
    val b = Vector(4f, 1f)
    val c = a + b
    println("${a} + ${b} = ${c}")

    var d = add(a, b)
    println(d)
    println("Last ${add.last}")
}
