package km.kotlin.java.lesson

data class Frame(var w: Int, var h: Int) {

    val size: Int
        get() {
            return w * h
        }

    var line: String = "dot-line"
        set(value) {
            field = value
        }

    var squere: Int
        get() = h
        set(value) {
            this.w = value
            this.h = value
        }
}

fun main() {
    val f = Frame(10,20)
    println("Frame size ${f.size}")
    f.squere = 100
    println("Frame size ${f.size}")
    println("Frame ${f}")
}