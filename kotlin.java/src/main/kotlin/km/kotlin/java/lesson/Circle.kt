package km.kotlin.java.lesson

class Circle {

    //@JvmOverloads
    constructor(centerX: Int, centerY: Int, radius: Double = 1.0) {
        println("x: ${centerX}, y: ${centerY}, r: ${radius}")
    }

    //@JvmOverloads
    fun draw(label: String, lineWidth: Int = 1, color: String = "RED") {
        println("l: ${label}, lw: ${lineWidth}, c: ${color}")
    }
}