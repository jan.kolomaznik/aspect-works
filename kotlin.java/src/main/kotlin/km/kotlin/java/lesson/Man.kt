package km.kotlin.java.lesson

data class Man(var name: String) {
    var age: Int = 0
}

fun main() {
    val man1 = Man("John")
    val man2 = Man("John")
    println("${man1} == ${man2}: ${man1 == man2}")
    man1.age = 10
    man2.age = 20
    println("${man1} == ${man2}: ${man1 == man2}")
    man1.name = "Honza"
    man2.name = "Jirka"
    println("${man1} == ${man2}: ${man1 == man2}")

}