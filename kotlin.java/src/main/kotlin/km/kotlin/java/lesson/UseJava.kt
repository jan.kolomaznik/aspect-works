package km.kotlin.java.lesson

fun main() {
    val person = Person("Pepa")
    println("Person ${person} has name: " + person.getName())

    person.name = "Jirka"
    println("Person has new name ${person.name}")


    println(person.`is`("Jojo"))
}

