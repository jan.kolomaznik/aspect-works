package km.kotlin.java.lesson

fun main() {

    fun build(title: String, width: Int, height: Int) {

    }

    build("PacMan", 400, 300)            // equivalent
    build(title = "PacMan", width = 400, height = 300)  // equivalent
    build(width = 400, height = 300, title = "PacMan")  // equivalent
    build("PacMan", width = 400, height = 300)      // equivalent

    //val p = Person(name = "Pepa")
}