package km.kotlin.java.lesson

data class Product(var price: Double) {
    companion object {
        @JvmField
        val COMPARATOR: Comparator<Product> = compareBy<Product> { it.price}

        @JvmStatic
        fun setSale(product: Product, sale: Double) {
            product.price *= (1.0-sale)
        }
    }
}