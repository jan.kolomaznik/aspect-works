package km.kotlin.java.lesson

interface Robot {
    /*@JvmDefault*/ fun move() { println("~walking~") }
    fun speak(): Unit
}