package km.kotlin.java.lesson

fun main() {
    var m = 1
    m = m.also { it + 1 }.also { it + 1 }
    println(m) //prints 1

    // let vs also
    data class Person(var name: String, var tutorial : String)
    var person = Person("Anupam", "Kotlin")

    var personLet = person.let { it.tutorial = "Swift"
                                 println("Log ${it}") }
    var personAlso = person.also { it.tutorial = "Android" }

    println("person $person")
    println("personLet $personLet")
    println("personAlso $personAlso")
}