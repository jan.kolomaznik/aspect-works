package km.kotlin.java.lesson

fun main() {
    data class Person(var name: String, var tutorial : String)

    var person1 = Person("Anupam", "Kotlin")

    person1.apply { this.tutorial = "Swift" }
    println(person1)

    var person2 = Person("Anupam", "Kotlin")

    person2.apply { tutorial = "Swift" }
    println(person2)

    person2.also { it.tutorial = "Kotlin" }
    println(person2)
}