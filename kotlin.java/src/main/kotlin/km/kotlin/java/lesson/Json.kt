package km.kotlin.java.lesson

import java.util.*

data class Coordinate(val lat: Double, val lng: Double)

fun main() {
    val coordinates = listOf(
            Coordinate(42.3, 12.3),
            Coordinate(52.1, 9.8)
    )
    val json = """
        {
          "id": "${UUID.randomUUID()}",
          "coordinates": [ ${coordinates.joinToString { c -> """
             [
                "lat": ${c.lat},
                "lng": ${c.lng}
             ]""" }}
          ]
        }
    """.trimIndent()

    println(json)
}
