package km.kotlin.java.lesson

fun main() {
    // Basic demo
    val x = 4
    val y = 7
    print("sum of $x and $y is ${x + y}")  // sum of 4 and 7 is 11

    // Object properties
    val person = Person("Pepa")
    println("Jmeno osoby: ${person.name}.")
    println("Role is host: ${person.`is`("Host")}")

    // Complex format by String
    val PI = Math.PI;
    println("PI value: %.2f".format(PI))

    // Kotlin bonus
    fun Any.foo(i: Int): String {
        return "Pecka"
    }
    println(PI.foo(1))
    fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)
    println("PI value: ${PI.format(4)}")
}