package km.kotlin.java.lesson

fun main() {
    var tutorial = "This is Kotlin Tutorial"
    println(tutorial) //This is Kotlin Tutorial

    tutorial = run {
        val tutorial = "This is run function"
        tutorial
    }
    println(tutorial) //This is run function

    // let and run
    var str : String? = null
    str?.let { println("p is $str") } ?:
         run { println("p was null.")
               str = "Kotlin"}

    println(str)
}