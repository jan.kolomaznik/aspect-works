package km.kotlin.java.lesson

import java.util.*


fun main() {
    val a = Person("Pepa")
    val b = Person("Pepa")

    println(a == b)  // Objects.equals(a, b)
    println(a === b) // a == b

    fun foo(i: Int = 0) : Int {
        return -i;
    }
}