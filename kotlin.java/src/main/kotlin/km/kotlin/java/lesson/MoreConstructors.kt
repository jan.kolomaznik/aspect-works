package km.kotlin.java.lesson

class MoreConstructors(val id: Any) {
    init {
        println(id)
    }
    constructor(i: Int) : this(i as Any) {
        println("Int construcotr")
    }
    constructor(d: Double) : this(d as Any) {
        println("double constructor")
    }
}

fun main() {
    val mci = MoreConstructors(1)
    val mcd = MoreConstructors(1.1)
}