package km.kotlin.java.lesson

fun main() {
    // Modify object
    var str = "Hello World"
    str = "$str!!"
    println(str)

    // Chaining let functions
    var a = 1
    var b = 2

    a = a.let { it + 2 }
         .let { val i = it + b; i}
    println(a)

    // Nesting let
    var x = "Anupam"
    x = x.let { outer ->
        outer.let { inner ->
            println("Inner is $inner and outer is $outer")
            "Kotlin Tutorials Inner let"
        }
        "Kotlin Tutorials Outer let"
    }
    println(x) //prints Kotlin Tutorials Outer let
}