package km.kotlin.java.lesson

fun main() {
    println((1..10).filter { it % 2 == 0 })
    println((1..10).filter { it -> it % 2 == 0 })
}