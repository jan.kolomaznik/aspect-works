package km.kotlin.java.lesson

data class Book(val title: String, val autor: String, val pages: Int)

fun main() {
    val hobit = Book("Hobit", "Tolkien", 256)
    val reprintHobit = hobit.copy(pages = 320)

    println("Origin hobit: ${hobit}")
    println("Reprint hobit: ${reprintHobit}")
    println("Same references: ${hobit === reprintHobit}")


    val (title, autor, pages) = hobit
    println(title)
    println(autor)
    println(pages)


}