package km.kotlin.java.lesson

class User {
    var name: String? = null
    fun foo(): String {
        return "Hello from ${name}"
    }
}