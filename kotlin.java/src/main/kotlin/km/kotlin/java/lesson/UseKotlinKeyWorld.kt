package km.kotlin.java.lesson

fun main() {
    val person = Person("Carl")
    println("Person ${person.name} is CUSTOMER: " + person.`is`("CUSTOMER"))
}

