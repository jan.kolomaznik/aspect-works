package km.kotlin.java.lesson;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class JsonJava {

    public static void main(String[] args) {
        List<Coordinate> coordinates = Arrays.asList(
                new Coordinate(1.0, 2.0),
                new Coordinate(3.0, 4.0),
                new Coordinate(5.0, 6.0));

        String str = coordinates.stream()
                .map(Coordinate::toString)
                .collect(Collectors.joining(":", "<", ">"));
        System.out.println(str);
    }
}
