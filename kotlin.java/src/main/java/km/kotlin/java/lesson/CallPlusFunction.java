package km.kotlin.java.lesson;

public class CallPlusFunction {

    public static void main(String[] args) {
        System.out.format("1 + 2 = %d%n", MathFunctionKt.plus(1, 2));
    }
}
