package km.kotlin.java.lesson;

public class Konstruktors {

    static class InitOrderDemo{
        private final String firstProperty;
        private final String secondProperty;

        public InitOrderDemo(String name) {
            firstProperty = "First property: $name";
            //init {
            System.out.println("First initializer block that prints ${name}");
            //}
            secondProperty = "Second property: ${name.length}";
            //init {
            System.out.println("Second initializer block that prints ${name.length}");
            //}
        }
    }
}
