package km.kotlin.java.lesson;

public class UseKotlinProperties {

    public static void main(String[] args) {
        User user = new User();
        user.setName("Pepa");
        System.out.format("User name: %s%n", user.getName());
        System.out.println(user.foo());
    }
}
