package km.kotlin.java.lesson;

public class StaticKonstuktor {

    private static final String firstProperty = "First property: $name";
    static {
        System.out.println("First initializer block that prints ${name}");
    }
    private static final String secondProperty = "Second property: ${name.length}";
    static {
        System.out.println("Second initializer block that prints ${name.length}");
    }

    public static void main(String[] args) {
        System.out.println("Start");
    }
}
