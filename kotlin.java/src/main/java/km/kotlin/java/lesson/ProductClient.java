package km.kotlin.java.lesson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductClient {

    public static void main(String[] args) {
        Product car = new Product(300_000.0);
        List<Product> products = new ArrayList<>();
        products.add(car);
        products.add(new Product(150_000.0));

        // get Static filed
        Collections.sort(products, Product.COMPARATOR);
        System.out.format("Sorted product list %s%n", products);

        // call static method
        Product.setSale(car, 0.3);
        System.out.format("New car prise %.2f%n", car.getPrice());
    }
}
