package km.kotlin.java.lesson;

public class C3PO implements Robot {
    // move() implementation from Robot is available implicitly
    @Override
    public void speak() {
        System.out.println("I beg your pardon, sir");
    }

    public static void main(String[] args) {
        C3PO c3po = new C3PO();
        c3po.move(); // default implementation from the Robot interface
        c3po.speak();
    }

    @Override
    public void move() {

    }
}
