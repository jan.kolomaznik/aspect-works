[remark]:<class>(center, middle)
# Kotlin
![](media/kotlin.png)

[remark]:<slide>(new)
## Motivace
Java je jedním z nejpoužívanějších a nejstabilnějších objektově orientovaných jazyků pro backendové aplikace, což má své výhody i nevýhody.

Má obrovskou uživatelskou základnu, nespočet knihoven a frameworků, podporu všech hlavních operačních systémů.

Nicméně nese s sebou břímě v podobě nemožnosti radikálnějších změn v syntaxi, a to kvůli zpětné kompatibilitě.

To brání „odlehčení“ jazyka od zbytečného textu nebo jeho rychlejšímu vývoji jako takovému.

[remark]:<slide>(new)
## Nové jazyky
Kotlin je nový programovací jazyk od JetBrains, výrobce nejlepších světových IDE.

[remark]:<slide>(wait)
Každý rok se vytvářejí nové programovací jazyky, ale jen zřídkakdy získávají pozornost vývojářů, jakou má Kotlin v posledních několika letech.

[remark]:<slide>(wait)
Kotlin je univerzální, staticky napsaný programovací jazyk určený pro Java Virtual Machine, Android, prohlížeče a nativní řešení.

[remark]:<slide>(wait)
Vytvořil jej jazykový designér JetBrains **Andrey Breslav** a oficiálně jej společnost JetBrains oficiálně oznámila v roce 2011.

[remark]:<slide>(wait)
Dnes se Kotlin stal jedním z nejoblíbenějších jazyků na [Stack Overflow](https://insights.stackoverflow.com/survey/2019), jedním z nejrychleji rostoucích programovacích jazyků na GitHub a jazykem pro vývoj aplikací pro Android.


[remark]:<slide>(new)
## Proč je Kotlin dobrý:

![](media/image_1.png)

[remark]:<slide>(new)
### Kotlin kompiluje do JVM bajtkódu nebo JavaScriptu.
Není to jazyk, do kterého budete psát jádro operačního systému.

Největší zájem mají lidé, kteří dnes pracují s Javou, i když by to mohlo oslovit všechny programátory, kteří využívají runtime garbage collector.

Nebo programátorů používajících jazyky: Scala, Go, Python, Ruby a JavaScript.

![](media/image_2.png)

[remark]:<slide>(new)
### Kotlin pochází z průmyslu, ne z akademické obce.
Řeší problémy, kterým dnes čelí pracující programátoři.

Například typový systém vám pomůže vyhnout se výjimkám nulového ukazatele.

Výzkumné jazyky většinou nemají nulovou hodnotu, ale to není užitečné pro lidi, kteří pracují s velkými databázemi a API, které tak činí.

![](media/image_3.png)

[remark]:<slide>(new)
### Kotlin nic nepřijímá!
Myslím tím, že existuje vysoce kvalitní nástroj pro převod Java na Kotlin na jedno kliknutí a silné zaměření na binární kompatibilitu Java.

Existující projekt Java můžete převádět najednou a vše se bude stále kompilovat, a to i u složitých programů, které běží na milionech řádků kódu.

Takto adoptuji Kotlin a očekávám, že to bude to, co dělá většina vývojářů.

![](media/image_4.png)

[remark]:<slide>(new)
### Kotlin programy používat všechny stávající Java knihovny.
Jako zřejmý důsledek výše uvedeného mohou, a dokonce můžete používat i vaše oblíbené frameworky, jako například Sprint Boot.

Interoperabilita je jednoduchá a nevyžaduje obálky ani vrstvy adaptérů.

Integruje se s Maven, Gradle a dalšími systémy sestavení.

![](media/image_5.jpeg)

[remark]:<slide>(new)
### Je snadný na naučení
Je přístupný a lze jej naučit během několika hodin pouhým přečtením jazykové reference.

Syntaxe je štíhlá a intuitivní.

Kotlin vypadá hodně jako Scala, ale je jednodušší.

Jazyk dobře vyvažuje napjatost a čitelnost.

![](media/image_6.png)

[remark]:<slide>(new)
### Je multiparadigmatický
Vynucuje žádnou zvláštní filozofii programování, jako je příliš funkční nebo styl OOP.

![](media/image_7.jpg)

[remark]:<slide>(new)
### Neukládá režijní náklady za běh.
Standardní knihovna je malá a pevná: skládá se většinou ze soustředěných rozšíření standardní knihovny Java.

Hodně používá kompilace optimalizace v době překladu.

![](media/image_8.png)

[remark]:<slide>(new)
### Kotlin začíná být populární u vývojářů Androidu.
Společnost google jej představila jako hlavní programovací jazyk pro Android.

V kombinaci s výskytem rámců jako Anko a Kovenant, tato lehkost zdrojů znamená, že  Pokud pracujete na Androidu, brzy budete v dobré společnosti.

[Můžete si přečíst zprávu napsanou o zkušenostech s Kotlinem a Androidem](https://docs.google.com/document/d/1ReS3ep-hjxWA8kZi0YqDbEhCqTt29hG8P44aA9W0DM8/edit?hl=en&forcehl=1).

![](media/image_9.jpg)

[remark]:<slide>(new)
### Kotlin vám umožní nadále používat nástroje zvyšující produktivitu.
Pokud používáte IntelliJ, IDE interop je zcela bezproblémový: kód lze refactored, prohledávat, navigovat a automaticky dokončovat, jako by Kotlin kód byl Java a naopak.

K dispozici je plná podpora pro ladění, testování jednotek, profilování atd.

![](media/image_a.jpg)

[remark]:<slide>(new)
### kotlin je vhodný pro Enterprise Java
Kromě Androidu si myslím, že Kotlin je velmi vhodný pro J2EE.

Protože:
- **Má silnou obchodní podporu od zavedené společnosti**:
 JetBrains se angažuje v projektu, má na něm velký a vysoce kompetentní tým, má stabilní obchodní model a dokonce převádí části svého vlastního vlajkového produktu, aby jej mohl používat.
 Je nepravděpodobné, že bude Kotlin brzy opuštěn.

- **Přijetí Kotlin je nízké riziko**: může být vyzkoušeno v malé části vaší kódové základny jedním nebo dvěma nadšenými členy týmu, aniž by to narušilo zbytek vašeho projektu.
 Kotlinské třídy exportují Java API, které vypadá stejně jako běžné Java kódy.

- **Protože se Kotlin zaměřuje na čitelnou syntaxi**:
 recenze kódu nejsou problémem, stále je mohou provádět členové týmu, kteří tento jazyk neznají.

- **Kotlin od verze 1.3.30 je plně kompatibilní s JVM 11**:
 takže ji můžete použít, i když vaše implementace ztěžuje přechod na novější JVM.


[remark]:<slide>(new)
## Funkce
Kotlin vyniká v moři nových programovacích jazyků díky svému zaměření na ekosystém: JetBrains chápe, že produktivita pochází z více než pohodlné syntaxe.

Proto má Kotlin mnoho užitečných funkcí, díky nimž je psaní kódu příjemné.

[remark]:<slide>(wait)
### Null bezpečnost
Již jsme zmínili null bezpečnost (volitelnost), která umožňuje kompilátoru systematicky označovat potenciální nulové odkazy ukazatele.

Na rozdíl od některých jazyků se nejedná o typ `Option`, a proto sebou nese nulovou režie.

[remark]:<slide>(new)
### Štíhlá syntaxe
* rozhraní typu funguje všude,
* jednořádkové funkce mají skutečně jeden řádek
* jednoduché struktury / JavaBeans lze také deklarovat na jedné řádce.
* Pro vlastnosti generují getter/setter metody.
* Funkce mohou existovat mimo třídy.

[remark]:<slide>(wait)
### Výjimky
Výjimky nejsou **kontrolované**.

[remark]:<slide>(wait)
### Anotace data
Přidání **anotace `data`** ke třídě spustí autogenerování metod jak `equals`, `hashCode`, `toString` a metod kopírování.

Takto vytváří pohodlně neměnné třídy bez potřeby builderů.

[remark]:<slide>(new)
### Funkcionálního programování
Podpora **funkcionálního programování** s lambda funkcemi a schopností provádět mapování, skládání atd. v Java Collection API.

Kotlin rozlišuje mezi modifikovatelný a nemodifikovatelný kolekcemi.

[remark]:<slide>(wait) 
### Rozšiřování objektů
Možnost **přidávat funkce do tříd** aniž by se změnil jejich zdrojový kód.

Nejprve to vypadá jako povrchní kousek syntaktického cukru, aby se zabránilo třídám stylu FooUtils.

Pak si uvědomíte, že tímto způsobem vám umožní snadno najít nové metody pomocí automatického doplňování, umožňuje vám vytvářet výkonná rozšíření a umožňuje vám integrovat stávající Java API s dalšími funkcemi do Kotlinu.

[remark]:<slide>(new)
### Přetížení operátorů

Ale ne systémem Scala/Perl.

Operátory se mapují podle speciálních názvů metod, takže mohou potlačit chování existujících operátorů (včetně vyvolání funkce), **ale nemůžete definovat úplně nové**.

Tím se dosáhne rovnováhy mezi výkonem a čitelností.

[remark]:<slide>(wait)
### Rozšiřování jazyka
Kotlin **nemá makra** ani jiné způsoby, jak předefinovat jazyk, ale kolekce pečlivě navržených funkcí umožňuje knihovnám, fungovat rozšíření jazyka.

*Příklad:* chcete používat [fibers, actors, and Go-style channels](http://blog.paralleluniverse.co/2015/06/04/quasar-kotlin/)? Knihovna zvaná Quasar vám to umožní.

[remark]:<slide>(new)
### Dokumentace
**Markdown místo HTML** pro dokumentaci API.

Díky tomu je psaní JavaDoců mnohem příjemnější.

Nástroj Dokka, který je ekvivalentem JavaDoc, umí číst zdrojový kód Kotlin i Java a generovat kombinované webové stránky doc, a to jak svým vlastním stylem, tak i ve standardním stylu JavaDoc HTML.

[remark]:<slide>(wait)
###  Lepší generiky
Pokud jste se nikdy plně nezvládli s tím, co přesně `super` a `extends` v generikách znamená, Kotlin je nepoužívá.

[remark]:<slide>(wait)
### Delegování
Delegování metod lze provádět automaticky.

[remark]:<slide>(new)
### Operátor rovná se
Operátor == provede to, co skutečně očekáváte.

[remark]:<slide>(wait)
### Asynchronní programování
Podpora **asynchronní programování**? Samozřejmě, že ano.

[remark]:<slide>(wait)
### Řetězcová interpolace
Tedy vkládání hosnota do řetězců: `„funguje jako ${this.example}!“`

[remark]:<slide>(wait)
### Argumenty funkcí
Argumenty funkcí mohou být pojmenovány, volitelné a variadic.

[remark]:<slide>(new)
## Hello world
Prvním programem pro vstup do jazyka je "Hello world"

1. Vytvořte *Gradle* projekt pro Kotlin.

2. Vytvořte v adresáři `src/main/kotlin` soubor `HelloWorld.kt`

3. Vložte následující kousek kódu:

```kotlin
fun main() {
   println("Hello World!")
}
```

g