KOTLIN PRO JAVA VÝVOJÁŘE
========================

- **Lektor:** Ing. Jan kolomazník, Ph.D.
- **Mobil:** +420 732 568 669
- **Email:** jan.kolomaznik@gmail.com 

Přepni z Javy do Kotlinu! Tento kurz je určen programátorům (primárně v jazyce Java), kteří se chtějí naučit „nejoblíbenější“ programovací jazyk současnosti Kotlin. 
Kotlin je 100% JVM kompatibilní jazyk. 
Díky tomu můžete pokračovat v práci na existujících programech psaných doposud v jazyce Java pomocí tohoto jazyka. 
V rámci školení se seznámíte s odlišnostmi oproti jazyku Java a zjistíte, jak Kotlin může zvýšit vaši produktivitu.


Osnova kurzu 
------------ 

- **[Co to je Kotlin](/kotlin)**
  * Historie a filozofie jazyka
  * Interoperabilita s Java
  * Překlad a spuštění kódu
  * Vývojové prostředí
- **[Co na kotlinu ocení Java vývojář](/kotlin.java)**
  * OOP
  * Datové třídy
  * Klíčová slova val a var
  * Null safety principy
  * Práce se Stringy, formátování
  * Chytré přetypování
  * Porovnávání (===)
  * Lambda výrazy
  * Cykly
  * Blok when
  * Properties
  * Defaultní a pojmenované argumenty
  * Externí funkce
  * Rozsahy
  * Přetěžování operátorů
