package com.example.demo.person

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import javax.validation.constraints.Min

data class PersonCreateRequest(
        var name: String,
        @Min(0)
        var age: Int?)

@RestController
@RequestMapping("/api")
class PersonRestController {

    @Autowired
    lateinit var personService: PersonService

    @GetMapping("/person")
    fun getPersons() = listOf(Person(), Person())

    @GetMapping("/person/{id}")
    fun getPersonById(@PathVariable id: Long) = Person()

    @PostMapping("/person")
    @ResponseStatus(HttpStatus.CREATED)
    fun postPerson(
            @RequestBody request: PersonCreateRequest) =
            personService.createPerson(
                    request.name,
                    request.age)


}