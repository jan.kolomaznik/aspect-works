package com.example.demo.person

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PersonService {

    @Autowired
    lateinit var personRepository: PersonRepository

    fun createPerson(name: String, age: Int?) =
            Person(name = name, age = age)
                    .also { println(it) }
                    .let { personRepository.save(it) }

}