package com.example.demo.person

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping

@Controller
@RequestMapping("/admin")
class PersonController {

    @Autowired
    lateinit var personRepository: PersonRepository

    @GetMapping("/person.html")
    fun person(model: MutableMap<String, Any>) : String {
        model["persons"] = personRepository.findAll()
        return "person/index"
    }
}