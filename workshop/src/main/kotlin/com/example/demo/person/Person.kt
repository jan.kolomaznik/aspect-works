package com.example.demo.person

import org.hibernate.annotations.Type
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Person(
        @Id
        @Type(type = "uuid-char")
        var id: UUID = UUID.randomUUID(),

        @Column(unique = true)
        var name: String = "None",

        @Column(nullable = true)
        var age: Int? = null)

