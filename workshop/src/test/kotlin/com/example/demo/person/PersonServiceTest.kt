package com.example.demo.person

import org.junit.Test

import org.junit.Assert.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
@DataJpaTest
class PersonServiceTest {

    @Autowired
    lateinit var repo: PersonRepository

    @Test
    fun `create Person basic test`() {
        val po = Person(name = "Pepa", age = 10)
        val ps = repo.save(po)
        assertTrue(po === ps)
    }
}